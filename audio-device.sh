#!/bin/bash
#lists audio devices: <index> <name>
function devices (){
    pacmd list-sinks | grep -A1 --no-group-separator index | sed 's/<//' | sed 's/>//' | sed 's/*/ /' | sed -r 's/\t+//g' | sed 'N;s/\n/ /' | awk -F ' ' '{print $2 "\t" $4}' | awk '{$1=$1};1'
}

#lists programms: <index> <sink> <name>
function programs (){
    pacmd list-sink-inputs | grep -e 'index\|sink:\|application.process.binary' |  sed 's/"//g' | sed -r 's/\t+//g' | sed 's/application.process.binary = //g' | sed 's/index: //g' | sed 's/sink: //g' | sed 's/<//' | sed 's/>//' | sed 'N;N;s/\n/ /g' | awk '{$1=$1};1' 
}

############################################################################## CHANGE IT!
icon=/usr/share/icons/Mint-X-Dark/status/24/audio-volume-high.png
#card1index = $default = default device, here Headphones/Speakers
card1="X-Fi"
card1index=""$(devices | grep "$card1" | awk -F ' ' '{print $1}')""
#card2index = secondary device, here BTSpeaker
card2="bluez"
card2index=""$(devices | grep "$card2" | awk -F ' ' '{print $1}')""
########################################################################################

if [ "$1" = "-h" ]; then
    echo "  PulseAudio output device toggle switcher"
    echo
    echo " audio-device [option1] [option2]"
    echo "  [Options 1]"
    echo "    none: Without options switches output devices for all applications"
    echo "    -f  : switches output device for ALL APPLICATIONS like focused window (run it via shortcut)"
    echo "  [Options 2]"
    echo "    -m  : switches output device for ALL APPLICATIONS like focused window (click)"
    echo
    echo "Devices:"
    echo "[Output Index] [Output Name]"
    devices
    echo
    echo "Running programs:"
    echo "[App Index] [Output Index] [Output device] [App Name]"
    programs
    echo
fi

apps_amount="$(programs | wc -l)"
default="$(devices | grep "$(pacmd stat | grep "sink name" | awk -F ' ' '{print $4}')" | awk -F ' ' '{print $1}')"

if [ "$1" = "-f" ]; then   

    if [ "$2" = "-m" ]; then

        appinfo=""$(xprop | grep -e "WM_CLASS\|_NET_WM_PID" | awk -F ' ' '{print $3}' | sed 's/"//g' | sed 's/,//g')""
        xappname=""$(echo $appinfo | awk -F ' ' '{print $2}')""
        xapppid=""$(echo $appinfo | awk -F ' ' '{print $1}')""
    else
        appinfo=""$(xprop -id $(xprop -root | awk '/_NET_ACTIVE_WINDOW\(WINDOW\)/{print $NF}') | grep -e "WM_CLASS\|_NET_WM_PID" | awk -F ' ' '{print $3}' | sed 's/"//g' | sed 's/,//g')""
        xappname=""$(echo $appinfo | awk -F ' ' '{print $2}')""        
        xapppid=""$(echo $appinfo | awk -F ' ' '{print $1}')""
    fi
    
    xappname2=""$(pacmd list-sink-inputs | grep -A3 "$xapppid" | tail -1 | awk -F '"' '{print $2}')""
    xapppid2=""$(pacmd list-sink-inputs | grep -B3 application.process.binary | grep -B3 "$xappname2" | head -1 | awk -F ' ' '{print $3}' | sed 's/"//g')""

# resolves conflicts when pids aint equal (pid of "general" process and audio using provess)
    if [ "$xapppid" = "$xapppid2" ]; then
        xappname="$xappname2"
    else
        xapppid="$xapppid2"
        xappname2="$xappname"
    fi

    xappindex=""$(programs | grep "$xappname" | awk -F ' ' '{print $1}')""
    xappsink=""$(programs | grep "$xappname" | awk -F ' ' '{print $2}' | head -1)""
    xappinstances=""$(echo $xappindex | sed 's/\s/\n/g' | wc -l)""

    if   [ "$xappsink" = "$card1index" ]; then
        for j in $( eval echo {1..$xappinstances} )
        do
            xappindex2=""$(echo $xappindex | sed 's/\s/\n/g' | sed -n "$j"p)""
            pacmd move-sink-input $xappindex2 $card2index
            notify-send -i $icon "Okno "$xappname" -> BT"
        done
    elif [ "$xappsink" = "$card2index" ]; then
        for j in $( eval echo {1..$xappinstances} )
        do
            xappindex2=""$(echo $xappindex | sed 's/\s/\n/g' | sed -n "$j"p)""
            pacmd move-sink-input $xappindex2 $card1index
            notify-send -i $icon "Okno "$xappname" -> USB"
        done
    fi
fi

if [[ -z "$1" ]];then
    if [ "$card1index" = "$default" ]
    then
    #line below is from stackoverflow :')
        for i in $( eval echo {1..$apps_amount} )
        do
            apps_sinks=""$(programs | awk -F ' ' '{print $1}' | sed -n "$i"p)""
            pacmd move-sink-input $apps_sinks $card2index
            pacmd set-default-sink $card2index
        done
        notify-send -i $icon "Output -> BT"
    else
        for i in $( eval echo {1..$apps_amount} )
        do
            apps_sinks=""$(programs | awk -F ' ' '{print $1}' | sed -n "$i"p)""
            pacmd move-sink-input $apps_sinks $card1index
            pacmd set-default-sink $card1index
        done
        notify-send -i $icon "Output -> USB"
    fi
fi